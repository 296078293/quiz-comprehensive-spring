package com.twuc.webApp.contract;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RequestGoods {


    @NotNull
    private String name;
    @NotNull
    private Double price;
    @NotNull
    private String unit;
    @NotNull
    private String url;

    public RequestGoods() {
    }

    public RequestGoods(String name, Double price, String unit, String url) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}
