package com.twuc.webApp.domain;


import javax.persistence.*;

@Entity
@Table(name = "item")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Goods goods;

    @Column
    private Integer quantity;

    public Item() {
    }

    public Item(Integer quantity) {
        this.quantity=quantity;
    }

    public Item(Goods goods, Integer quantity) {
        this.goods = goods;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Goods getGoods() {
        return goods;
    }


    public void setGoods(Goods goods) {
        this.goods = goods;
    }


}
