package com.twuc.webApp.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    boolean existsByGoodsId(Long goodsId);

    Item findByGoodsId(Long goodsId);
}
