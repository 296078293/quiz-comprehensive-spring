package com.twuc.webApp.handle;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

//@ControllerAdvice
public class HandleException {


    @ExceptionHandler({RuntimeException.class, MethodArgumentNotValidException.class})
    public ResponseEntity<String> handleException(Exception ex) {
        return ResponseEntity.status(400)
                .contentType(MediaType.APPLICATION_JSON)
                .body("Exception!");
    }
}
