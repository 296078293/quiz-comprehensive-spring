package com.twuc.webApp.service;

import com.twuc.webApp.contract.RequestGoods;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.GoodsRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class GoodsService {

    private final GoodsRepository goodsRepository;

    public GoodsService(GoodsRepository repository) {
        this.goodsRepository = repository;
    }

    public Goods saveGoods(RequestGoods requestGoods) {

        Goods goods = goodsRepository.saveAndFlush(
                new Goods(requestGoods.getName(), requestGoods.getPrice(),
                        requestGoods.getUnit(), requestGoods.getUrl()));
        return goods;
    }

    public List<Goods> findAll() {

        List<Goods> allGoods = goodsRepository.findAll();
        return allGoods;
    }

    public Optional<Goods> findById(Long id) {
        return goodsRepository.findById(id);
    }

    public void deleteById(Long id){
        Goods goods = findById(id).orElseThrow(RuntimeException::new);
        goodsRepository.delete(goods);
    }
}
