package com.twuc.webApp.service;

import com.twuc.webApp.domain.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {

    private final ItemRepository itemRepository;
    private final GoodsRepository goodsRepository;

    public ItemService(ItemRepository itemRepository, GoodsRepository goodsRepository) {
        this.itemRepository = itemRepository;
        this.goodsRepository = goodsRepository;
    }

    public Item createItem(Long id) {
        Goods goods = goodsRepository.findById(id).orElseThrow(RuntimeException::new);
        if (itemRepository.existsByGoodsId(goods.getId())) {
            Item item = itemRepository.findByGoodsId(goods.getId());
            item.setQuantity(item.getQuantity() + 1);
            return itemRepository.save(item);
        } else {
            Item item = new Item(goods, 0);
            return itemRepository.save(item);
        }
    }

    public Item findItemById(Long id) {
        Item item = itemRepository.findById(id).orElseThrow(RuntimeException::new);
        return item;
    }

    public void deleteItem(Long id) {
        itemRepository.deleteById(id);
        itemRepository.flush();
    }

    public List<Item> findItems() {
        return itemRepository.findAll();
    }
}
