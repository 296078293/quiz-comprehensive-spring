package com.twuc.webApp.web;

import com.twuc.webApp.contract.RequestGoods;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.GoodsRepository;
import com.twuc.webApp.service.GoodsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class GoodsController {

    private final GoodsService goodsService;

    private final GoodsRepository goodsRepository;

    public GoodsController(GoodsService goodsService, GoodsRepository goodsRepository) {
        this.goodsService = goodsService;
        this.goodsRepository = goodsRepository;
    }

    @PostMapping("/goods")
    ResponseEntity addGoods(@RequestBody @Valid RequestGoods requestGoods) {
        Goods goods = goodsService.saveGoods(requestGoods);
        return ResponseEntity.status(200).body(goods);
    }

    @GetMapping("/goods")
    ResponseEntity findAllGoods() {
        List<Goods> goods = goodsService.findAll();
        return ResponseEntity.status(200)
                .header("Content-Type", "application/json;charset=UTF-8")
                .body(goods);
    }

    @GetMapping("/goods/{id}")
    ResponseEntity<Goods> findGoodsById(@PathVariable Long id) {
        Goods goods = goodsService.findById(id).orElseThrow(RuntimeException::new);
        return ResponseEntity.status(200).body(goods);
    }

    @DeleteMapping("/goods/{id}")
    ResponseEntity deleteGoods(@PathVariable Long id) {
        goodsService.deleteById(id);
        return ResponseEntity.ok().build();
    }
}
