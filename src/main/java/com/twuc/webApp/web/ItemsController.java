package com.twuc.webApp.web;

import com.twuc.webApp.domain.Item;
import com.twuc.webApp.service.ItemService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class ItemsController {


    private final ItemService itemService;

    public ItemsController(ItemService itemService) {
        this.itemService = itemService;
    }

    @PostMapping("/items/{id}")
    ResponseEntity createItem(@PathVariable Long id) {
        Item item = itemService.createItem(id);
        return ResponseEntity.status(200).body(item);
    }

    @GetMapping("/items")
    ResponseEntity<List<Item>> findItems() {
        List<Item> items = itemService.findItems();
        return ResponseEntity.status(200).body(items);
    }

    @GetMapping("/items/{id}")
    ResponseEntity findItem(@PathVariable Long id) {
        Item item = itemService.findItemById(id);
        return ResponseEntity.status(200)
                .header("Content-Type", "application/json;charset=UTF-8")
                .body(item);
    }

    @Transactional
    @DeleteMapping("/items/{id}")
    ResponseEntity deleteItem(@PathVariable Long id) {
        itemService.deleteItem(id);
        return ResponseEntity.status(200).build();
    }


}
