package com.twuc.webApp;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contract.RequestGoods;
import com.twuc.webApp.domain.Goods;
import com.twuc.webApp.domain.GoodsRepository;
import com.twuc.webApp.domain.Item;
import com.twuc.webApp.domain.ItemRepository;
import com.twuc.webApp.web.IntegrationTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.persistence.EntityManager;


import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


class WebAppApplicationTest extends IntegrationTestBase {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    GoodsRepository goodsRepository;

    @Autowired
    ItemRepository itemRepository;
    @Autowired
    EntityManager entityManager;


    @Test
    void should_return_200_when_add_goods() throws Exception {

        RequestGoods goods = new RequestGoods("a1", 18.99, "buck", "www");
        final String requestJson = new ObjectMapper().writeValueAsString(goods);
        mockMvc.perform(post("/api/goods")
                .content(requestJson).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.name", is("a1")))
                .andExpect(jsonPath("$.price", is(18.99)));
    }

    @Test
    void should_return_400_when_add_goods_invalid() throws Exception {

        RequestGoods goods = new RequestGoods();
        final String requestJson = new ObjectMapper().writeValueAsString(goods);
        mockMvc.perform(post("/api/goods")
                .content(requestJson).contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_200_when_find_all_goods() throws Exception {
        Goods goods = new Goods("a1", 18.99, "buck", "www");
        Goods otherGoods = new Goods("a2", 38.99, "元", "www");
        goodsRepository.saveAndFlush(goods);
        goodsRepository.saveAndFlush(otherGoods);
        String contentAsString = mockMvc.perform(get("/api/goods"))
                .andExpect(status().is(200))
                .andReturn().getResponse().getContentAsString();
        System.out.println(contentAsString);
        ResponseGoodsForTest[] allGoods = new ObjectMapper().readValue(contentAsString, ResponseGoodsForTest[].class);
        assertEquals(2, allGoods.length);
        assertEquals(new Double(18.99), allGoods[0].getPrice());
    }

    @Test
    void should_return_200_when_delete_by_id() throws Exception {
        Goods goods = new Goods("a1", 18.99, "buck", "www");
        goodsRepository.saveAndFlush(goods);
        mockMvc.perform(delete("/api/goods/1"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_400_when_delete_by_id() throws Exception {
        Goods goods = new Goods("a1", 18.99, "buck", "www");
        goodsRepository.saveAndFlush(goods);
        mockMvc.perform(delete("/api/goods/3"))
                .andExpect(status().is(400));
    }


    @Test
    void should_return_200_when_create_orders() throws Exception {
        Goods goods = new Goods("a1", 18.99, "buck", "www");
        goodsRepository.saveAndFlush(goods);

        mockMvc.perform(post("/api/items/1"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.goods.name").value("a1"))
                .andExpect(jsonPath("$.quantity").value(1))
                .andExpect(jsonPath("$.id").value(1L));

        mockMvc.perform(post("/api/items/1"))
                .andExpect(status().is(200))
                .andExpect(jsonPath("$.goods.name").value("a1"))
                .andExpect(jsonPath("$.quantity").value(2))
                .andExpect(jsonPath("$.id").value(1L));
    }

    @Test
    void should_return_200_when_find_all_items() throws Exception {
        Goods goods = new Goods("a1", 18.99, "buck", "www");
        goodsRepository.saveAndFlush(goods);
        goods = new Goods("a2", 18.99, "buck", "www");
        goodsRepository.saveAndFlush(goods);
        mockMvc.perform(post("/api/items/2"));

        mockMvc.perform(get("/api/items")).andExpect(status().is(201));

    }

    @Test
    void should_return_200_when_delete_items() throws Exception {
        Goods goods = new Goods("a1", 18.99, "buck", "www");
        goodsRepository.saveAndFlush(goods);
        mockMvc.perform(post("/api/items/1")).andExpect(status().is(200));
        mockMvc.perform(delete("/api/items/1"));
        mockMvc.perform(get("/api/items/1")).andExpect(content().string("Exception!"))
                .andExpect(status().is(400));
        mockMvc.perform(get("/api/goods/1")).andExpect(jsonPath("$.name").value("a1"));
    }
}

class ResponseGoodsForTest {
    private Long id;
    private String name;
    private Double price;
    private String unit;
    private String url;
    private Item item;

    public Item getItem() {
        return item;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getUrl() {
        return url;
    }
}